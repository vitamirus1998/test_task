$(function() {
  let template = null
  getUsers()
  getCities()

  $("form").validate({
    ignore: ":hidden",
    rules: {
      name: {
        required: true,
        maxlength: 30,
        regexp: '^([a-zA-Zа-яА-Я ]+)$'
      },
      age: {
        required: true,
        digits: true,
        range : [0, 100]
      }

    },
    messages: {
      name: {
        required: "Поле обязательно для заполнения",
        maxlength: "Длина имени должна быть меньше 30-ти символов",
        regexp: 'Имя должно состоять только из букв'
      },
      age: {
        required: "Поле обязательно для заполнения",
        digits: "Возраст может быть только положительным",
        range: "Возраст может быть от 0 до 100",
      }
    },
    submitHandler: function (form) {
      $.ajax({
        type: "POST",
        url: '../routers/make-data.php',
        data: $(form).serialize()+'&add=true',
        success: function(data){
          $('#user-form').trigger('reset')
          getUsers()
        }
      });
    }
  })

  jQuery.validator.addMethod(
      'regexp',
      function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
      },
  );



  function getUsers() {
    $.ajax({
      url: '/routers/send-users.php',
      type: 'GET',
      success: function(response) {
        const user = JSON.parse(response);
        let template = '';
        user.forEach(user => {
          if (user.city === null) {
            user.city = '-город не выбран-'
          }
          template += `
                    <tr data-id="${user.id}">
                      <td>${user.id}</td>
                      <td class="name" data-value="${user.name}">${user.name}</td>
                      <td class="age" data-value="${user.age}">${user.age}</td>
                      <td class="city city_id">${user.city}</td>
                      <td>
                        <button class="user-delete btn btn-danger">
                         Удалить
                        </button>
                      </td>
                    </tr>
                  
                `
        });
        $('#user').html(template);
      }
    });
  }

  function getCities() {
    $.ajax({
      url: '../routers/send-cities.php',
      type: 'GET',
      success: function(response) {
        const city = JSON.parse(response);
        template = `<option value="">-город не выбран-</option><hr>`
        city.forEach(city => {
          template += `<option value="${city.id}">${city.name}</option>`


        });
        $('select').html(template);
      }
    });
  }

  function axiosphp (dataId, fieldName, tag) {
    $.ajax({
      type: "POST",
      url: '../routers/make-data.php',
      data: {update: {value: $(tag).val() ,id: dataId, field: fieldName}},
      success: function(data){
        let ajax = $('.ajax')
        ajax.data('value', $('.ajax input').val())
        ajax.html($('.ajax input').val());
        ajax.removeClass('ajax');
      }
    })
  }

  function axiosphp_name(tag){
    let fieldName = $(tag).data('fieldname');
    let dataId = $(tag).data('id');
    let regex = RegExp('^([a-zA-Zа-яА-Я ]+)$')
    if ($(tag).val() !== '' && $(tag).val().length < 30 && regex.test($('.ajax input').val())){
      axiosphp(dataId, fieldName, tag)
    } else {
      let ajax = $('.ajax')
      ajax.html(ajax.data('value'));
      ajax.removeClass('ajax');
    }
  }

  function axiosphp_age(tag){
    let dataId = $(tag).data('id');
    let fieldName = $(tag).data('fieldname');
    let regex = RegExp('^([0-9]+)$')
    if ($(tag).val() !== '' && Number.parseInt($(tag).val()) < 100 && regex.test($('.ajax input').val())){
      axiosphp(dataId, fieldName, tag)
    } else {
      let ajax = $('.ajax')
      ajax.html(ajax.data('value'))
      ajax.removeClass('ajax')
    }
  }

  $(document).on('click', 'td.name', function () {

    if ($("#editboxname").length > 0) {
      return false;
    }

    let tdClasses = $(this).attr('class').split(" ");
    let fieldName = tdClasses[0];
    let dataId = $(this).parent('tr').data('id');
    $(this).addClass('ajax').html('<input  data-id="'+dataId+'" data-fieldname="'+fieldName+'" class="form-control" id="editboxname" type="text" value="'+$(this).text()+'" />');
    $('#editboxname').focus();

  });

  $(document).on('click', 'td.age', function () {

    if ($("#editboxage").length > 0) {
      return false;
    }

    let tdClasses = $(this).attr('class').split(" ");
    let fieldName = tdClasses[0];
    let dataId = $(this).parent('tr').data('id');
    $(this).addClass('ajax').html('<input  data-id="'+dataId+'" data-fieldname="'+fieldName+'" class="form-control" id="editboxage" type="text" value="'+$(this).text()+'" />');
    $('#editboxage').focus();

  });

  $(document).on('click', 'td.city',
      function() {
        if ($("#editboxcity").length > 0) {
          return false;
        }
        let city_name = $(this).text().replace(/\s+/g, '')
        let tdClasses = $(this).attr('class').split(" ");
        let fieldName = tdClasses[1];
        let dataId = $(this).parent('tr').data('id');
        $(this).addClass('ajax-city').html('<select data-id="'+dataId+'" data-fieldname="'+fieldName+'" class="form-control" id="editboxcity" >'+template+'</select>');
        $('#editboxcity').focus();
        $(this).find(`option:contains("${city_name}")`).prop('selected', true)
      }
  )

  $(document).on('mouseleave', '#editboxname', function (e) {
    axiosphp_name(this)
  })


  $(document).on('keydown', '#editboxname', function (e) {

    if (e.keyCode == 13) {
      axiosphp_name(this)
    }

  });


  $(document).on('mouseleave', '#editboxage', function (e) {
    axiosphp_age(this)
  })


  $(document).on('keydown', '#editboxage', function (e) {
    if (e.keyCode == 13) {
      axiosphp_age(this)
    }

  });

  $(document).on('mouseleave', '#editboxcity', function (e) {

    let fieldName = $(this).data('fieldname');
    let dataId = $(this).data('id');
    $.ajax({
      type: "POST",
      url:"../routers/make-data.php",
      data: {value:$(this).val(),id:dataId,field:fieldName},
      success: function(data){
        let ajax_city = $('.ajax-city')
        let text = $('.ajax-city select').find('option:selected').text()
        if (text === null) {
          ajax_city.html('-город не выбран-');
        } else {
          ajax_city.html(text);
        }
        ajax_city.removeClass('ajax-city');
      }
    });
  });

  $(document).on('click', '.user-delete', function(e) {
    const id = $(this).parent().parent('tr').data('id');
    $.ajax({
      url: '../routers/make-data.php',
      type: 'POST',
      data: {delete: {id: id}},
      success: function(data){
        getUsers()
      }
    })
  })
})
