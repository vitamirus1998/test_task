<?php

require_once '../controllers/UserController.php';

if(isset($_POST['add'])) {
    $data = new UserController();
    $data->add($_POST);
}

if(isset($_POST['update'])) {
    $data = new UserController();
    $data->update($_POST['update']);
    return json_encode($_POST);
}

if(isset($_POST['delete'])) {
    $data = new UserController();
    $data->delete($_POST['delete']);
}