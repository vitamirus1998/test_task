<?php

class Database{
    public static $host = 'localhost';
    public static $user = 'root';
    public static $password ='root';
    public static $database = 'test';
    public static $connection = null;

    public function __construct(){
        self::$connection = mysqli_connect(
            self::$host, self::$user, self::$password, self::$database
        ) or die("Ошибка " . mysqli_error(self::$connection));

    }


}