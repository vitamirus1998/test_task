<?php

include_once '../settings/Database.php';

class City extends Database
{


    // Установление соединения с базой данных
    public function __construct()
    {
        self::$connection = mysqli_connect(
            self::$host, self::$user, self::$password, self::$database
        ) or die("Ошибка " . mysqli_error(self::$connection));
    }

    // Получение списка городов
    public function list()
    {
        $query = "SELECT * from  cities ";
        $result = mysqli_query(self::$connection, $query) or die("Ошибка " . mysqli_error(self::$connection));
        $json = array();
        while ($row = mysqli_fetch_array($result)) {
            $json[] = array(
                'id' => $row['id'],
                'name' => $row['name_city'],
            );
        }
        return $json;
    }

    // Закрытие соединения с бд
    public function __destruct()
    {
        mysqli_close(self::$connection);
    }
}