<?php

include_once '../settings/Database.php';

class User extends Database {

    protected $id = null;
    public $name = null;
    public $age = null;
    public $city_id = null;

    // Соединение с бд и если id пользователя не null то его получение
    public function __construct($id=null){
        $this->id = $id;
        self::$connection = mysqli_connect(
            self::$host, self::$user, self::$password, self::$database
        ) or die("Ошибка " . mysqli_error(self::$connection));
        if ($id != null) {
            $query = "SELECT name, age, city_id FROM users WHERE id = '$id'";
            $result = mysqli_query(self::$connection, $query) or die("Ошибка " . mysqli_error(self::$connection));

            while($row = mysqli_fetch_array($result)) {
                $this->name =  $row['name'];
                $this->age =  $row['age'];
                $this->city_id =  $row['city_id'];
            }
        }
    }

    public function list() {
        $query = "SELECT users.id, users.name, users.age, cities.name_city from users LEFT JOIN cities ON users.city_id = cities.id";
        $result = mysqli_query(self::$connection, $query) or die("Ошибка " . mysqli_error(self::$connection));
        $json = array();
        while($row = mysqli_fetch_array($result)) {
          $json[] = array(
              'id' => $row['id'],
              'name' => $row['name'],
              'age' => $row['age'],
              'city' => $row['name_city']
            );
          }
        return $json;
    }
    // Добавление нового пользователя
    public function add() {
        if ($this->city_id == null){
            $query = "INSERT into users VALUES (NULL ,'$this->name', '$this->age', NULL)";
        }
        else {
            $query = "INSERT into users VALUES (NULL ,'$this->name', '$this->age', '$this->city_id')";
        }
        $result = mysqli_query(self::$connection, $query) or die("Ошибка " . mysqli_error(self::$connection));
    }

    // Редактирование пользователя
    public function update($fieldName){
        $value = $this->$fieldName;
        $query = $value != null
            ? "UPDATE users SET $fieldName = '$value' WHERE id = $this->id"
            : "UPDATE users SET $fieldName = NULL WHERE id = $this->id";
        $result = mysqli_query(self::$connection, $query) or die("Ошибка " . mysqli_error(self::$connection));
    }

    public function delete() {
        $query = "DELETE FROM users WHERE id = '$this->id'";
        $result = mysqli_query(self::$connection, $query) or die("Ошибка " . mysqli_error(self::$connection));
    }

    // Закрытие соединения с бд
    public function __destruct()
    {
        mysqli_close(self::$connection);
    }
}
