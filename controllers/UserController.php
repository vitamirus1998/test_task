<?php

require_once '../settings/Database.php';
require_once '../models/User.php';


class UserController extends Database {

    public function list() {
        $user = new User();
        return json_encode($user->list());
    }

    public function add($data) {
        $user_name = htmlentities(mysqli_real_escape_string(self::$connection, $data['name']));
        $user_age = (int)htmlentities(mysqli_real_escape_string(self::$connection, $data['age']));
        $user_city = (int)htmlentities(mysqli_real_escape_string(self::$connection, $data['city']));

        $user = new User();
        $user->name = $user_name;
        $user->age = $user_age;
        $user->city_id = $user_city;
        $user->add();

        return $data;
    }

    public function update($data) {
        $fieldName = htmlentities(mysqli_real_escape_string(self::$connection, $data['field']));
        $value = htmlentities(mysqli_real_escape_string(self::$connection, $data['value']));
        $id = (int)htmlentities(mysqli_real_escape_string(self::$connection, $data['id']));

        $user = new User($id);
        $user->{$fieldName} = $value;
        $user->update($fieldName);
    }

    public function delete($data) {
        $id = (int)htmlentities(mysqli_real_escape_string(self::$connection, $data['id']));
        $user = new User($id);
        $user->delete();

    }

}