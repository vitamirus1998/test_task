<?php

require_once '../models/City.php';


class CityController
{
    public function list() {

        $user = new City();
        return json_encode($user->list());
    }

}