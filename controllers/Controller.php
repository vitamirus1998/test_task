<?php

class Controller extends Database{
    protected function __construct(){
        self::$connection = mysqli_connect(
            self::$host, self::$user, self::$password, self::$database
        ) or die("Ошибка " . mysqli_error(self::$connection));
    }
}